TITRE DU PROJET : RETRAIT D'AFGHANISTAN

FORMAT : Webdocumentaire

PUBLICATION : Liberation.fr

ANNEE : 2012

REPORTAGE : Adrian Branco

CODE & DESIGN : Guillaume Garvanèse

MUSIQUE : Kenma Shindo

DETAILS : http://www.garvanese.com/2012/07/retrait-afghanistan-nouveau-webdoc-publie-par-liberation.html

LICENCES :
- Code source : CC-BY Guillaume Garvanèse
- Matériel graphique et audiovisuel : Copyright Adrian Branco / Guillaume Garvanèse / Kenma Shindo